// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SceneController.generated.h"

/**
The actor stores the variables that are used in the scene.
*/
UCLASS()
class TESTWORK_API ASceneController : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASceneController();

	/** Pawn on stage */
	UPROPERTY(EditAnywhere)
	APawn* Pawn;

	/** Camera on stage */
	UPROPERTY(EditAnywhere)
	ACameraActor* CameraActor;
	
};
