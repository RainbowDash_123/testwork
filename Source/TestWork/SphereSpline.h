// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "Components/SplineComponent.h"
#include "GameFramework/Actor.h"
#include "SphereSpline.generated.h"

UCLASS()
class TESTWORK_API ASphereSpline : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASphereSpline();

	UPROPERTY(VisibleAnywhere, Category = "Components")
	USceneComponent* MyRootComponent;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	USplineComponent* SplineComponent;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	UStaticMeshComponent* Sphere;

	/** The speed of movement of the sphere along the spline */
	UPROPERTY(EditAnywhere, Category = "Settings")
	float Speed = 500.f;

	UPROPERTY()
	float Distance;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
