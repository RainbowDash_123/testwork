// Fill out your copyright notice in the Description page of Project Settings.


#include "TestPlayerController.h"

#include "SceneController.h"
#include "TestPawn.h"
#include "Blueprint/UserWidget.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "Camera/CameraActor.h"
#include "Kismet/GameplayStatics.h"

void ATestPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if(auto NewMenuWidget = CreateWidget(this, MenuWidgetClass))
	{
		NewMenuWidget->AddToViewport();
		bShowMouseCursor = true;
		FInputModeGameAndUI InputModeGameAndUI;
		InputModeGameAndUI.SetWidgetToFocus(NewMenuWidget->TakeWidget());
		SetInputMode(InputModeGameAndUI);
	}
	
	if(auto Controller = Cast<ASceneController>(UGameplayStatics::GetActorOfClass(this, ASceneController::StaticClass())))
	{
		Possess(Controller->Pawn);
		SetViewTargetWithBlend(Controller->CameraActor);
	}
}

void ATestPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAction("Speed_1", EInputEvent::IE_Pressed, this, &ATestPlayerController::ChangeSpeed_Helper<1>);
	InputComponent->BindAction("Speed_2", EInputEvent::IE_Pressed, this, &ATestPlayerController::ChangeSpeed_Helper<2>);
	InputComponent->BindAction("Speed_3", EInputEvent::IE_Pressed, this, &ATestPlayerController::ChangeSpeed_Helper<3>);
	InputComponent->BindAction("Speed_4", EInputEvent::IE_Pressed, this, &ATestPlayerController::ChangeSpeed_Helper<4>);
}

void ATestPlayerController::ChangeSpeedPawn(float value)
{
	if(auto MyPawn = Cast<ATestPawn>(GetPawn()))
	{
		MyPawn->SetSpeed(Speeds.IsValidIndex(value) ? Speeds[value - 1] : 0);
	}
}